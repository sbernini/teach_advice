<?php

namespace App\DataFixtures;

use App\Entity\Tip;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $tips = $manager->getRepository(Tip::class)->findAll();
        foreach($tips as $tip)
        {
            $manager->remove($tip);
        }
        $manager->flush();

        foreach($this->getPhrases() as $phrase)
        {
            $tip = new Tip();
            $tip->setContent($phrase);

            $manager->persist($tip);
        }

        $manager->flush();
    }

    private function getPhrases(): array
    {
        return [
            'Rendi divertente una lezione',
            'Dai ai tuoi allievi quello per cui sono venuti al tuo corso',
            'Fai lezione per tutto il tempo che ti è stato pagato',
            'Esprimi apprezzamento per l\'impegno dei tuoi allievi',
            'Proponi ai tuoi allievi materiale di studio aggiuntivo (libri, video, musica, ecc..)',
            'Non dare i tuoi allievi per scontati',
            'Tratta tutti gli allievi allo stesso modo',
            'Lascia negatività e malumore a casa',
            'Rendi una lezione il momento migliore della giornata dei tuoi allievi',
            'Rendi la tua scuola di danza un ambiente piacevole in cui stare',
            'Prepara corsi e lezioni in anticipo',
            'Consiglia ai tuoi studenti altri insegnanti e altre scuole, quando questo è nei loro migliori interessi',
            'Trova metodi efficaci (e gentili) per correggere i tuoi allievi',
            'Non metterti in mostra senza un buon motivo',
            'Non inventare scuse',
            'Non soffermarti sugli errori',
            'Crea degli "extra" per studenti capaci/volenterosi',
            'Sii non giudicante',
            'Mostrati sempre con un aspetto curato',
            'Sii sempre in orario',
            'Ogni lezione dovrebbe sempre avere un obiettivo chiaro',
            'Per ogni figura/amalgamazione, crea diverse opzioni tra cui scegliere',
            'Credi nei tuoi allievi, e sul fatto che ognuno di essi possa migliorarsi e avere successo',
            'Cerca di ricordarti quello di cui i tuoi allievi ti parlano (nomi, fatti, ecc..)',
            'Fai pratica di "Ascolto attivo"',
            'Cerca di capire quando è il momento di chiedere impegno (e quando non lo è)',
            'Studia e sviluppa diversi metodi di insegnamento',
            'Scegli il miglior sistema di insegnamento possibile',
            'Adattati ai diversi modi di apprendere che ogni persona ha',
            'Non sprecare il tempo dei tuoi allievi',
            'Non perdere tempo in pettegolezzi',
            'Usa musica che sia appropriata per il livello dei tuoi allievi',
            'Tieni traccia/registra il materiale fatto a lezione',
            'Cerca di ottenere certificazioni davvero professionali (che diano un vero valore aggiuntivo ai tuoi allievi)',
            'Fai pratica di discipline complementari (yoga, pilates, ecc...)',
            'Cerca di dare sempre il 100% di te stesso/a ai tuoi studenti',
            'Insegna in maniera incrementale: prima le basi',
            'Dimostra ai tuoi studenti che tieni a loro',
            'Scusati per i tuoi errori',
            'Cerca di valere quello per cui sei pagato...e anche molto di più',
            'Cerca di stimolare fiducia e autostima nei tuoi allievi',
            'Sii oggi un\'insegnante migliore di ieri',
            'Studia i principi di comunicazione e costruzione di relazioni con altre persone',
            'Incoraggia l\'amicizia tra i tuoi allievi',
            'Non avere studenti preferiti',
            'Revisiona e correggi le figure che fanno i tuoi allievi',
            'Crea delle amalgamazioni ben studiate a tavolino',
            'Ruota le coppie di frequente e in maniera ben organizzata',
            'Sii rispettoso/a del tuo partner di ballo',
            'Impara a insegnare il Lead&Follow in molti modi differenti',
            'Impara la terminologia specifica di ballo',
            'Impara ad eseguire/insegnare "giri e piroette"',
            'Non parlare male di altre scuole di ballo, come insegnante non è professionale. Punta sui tuoi meriti, piuttosto che sui demeriti degli altri',
            'La concorrenza di altre scuole può stimolare crescita e miglioramento personali: è nel miglior interesse dei tuoi allievi',
            'I tuoi allievi hanno il diritto di gestire il tempo fuori dalle lezioni come meglio credono',
            'I tuoi allievi non sono una tua proprietà: sono sempre liberi di cambiare corso o scuola, se lo ritengono utile',
            'Continua a studiare e mantenerti aggiornato: il ballo si evolve, e così dovresti fare anche tu',
            'Impara con precisione il footwork di ogni figura insegnata',
            'Impara la storia di ogni ballo che stai insegnando',
            'Impara a contare i battiti/beat di ogni figura',
            'Impara i concetti base di teoria musicale',
            'Studia le caratteristiche musicali della musica utilizzata a lezione',
            'Impara a "contare musicalmente"',
            'Studia l\'anatomia umana',
            'Impara a insegnare elementi di consapevolezza corporea',
            'Crea delle fasi di riscaldamento divertenti',
            'Dai feedback chiari, semplici e costruttivi',
            'Impara a insegnare "come creare Bounce" (e i diversi modi per farlo)',
            'Impara a insegnare "come creare un Frame" (e come gestirlo)',
            'Impara le diverse posizioni di ballo (neutra, counter-balance, ecc..)',
            'Impare i concetti base di connessione: come crearla, utilizzarla e le diverse intensità che si possono avere',
            'Sii onesto/a con i tuoi allievi riguardo a quello che sai, e quello che non sai',
            'Sii creativo/a',
            'Sii gentile con allievi e collaboratori',
            'Sii empatico/a, di fronte alle difficoltà di allievi e collaboratori',
            'Sii "gender-neutral" relativamente alle coppie di ballo',
            'Evita temi divisivi(religione, politica, denaro, ecc..)',
            'Mantieni il miglior interesse per i tuoi allievi come obiettivo principale',
            'Ogni lezione dovrebbe essere un\'esperienza divertente e memorabile',
            '"Saper fare", "sapere" e "saper insegnare", sono tre mestieri molto diversi tra loro...sii competente in ognuno di essi',
            'Studia didattica e i relativi metodi applicati alla danza',
            'Ispirati e impara dai tuoi insegnanti preferiti',
            'Cerca di definire con precisione quello che per te è "tecnica", e quello che per te è "stile"',
            'C\'è sempre qualcosa di nuovo da imparare, e c\'è sempre spazio per migliorare le proprie competenze',
        ];
    }
}
