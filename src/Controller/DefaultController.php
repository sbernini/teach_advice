<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\TipRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name:'_welcome')]
    public function default(TipRepository $tipRepository): Response
    {
        return $this->render('tip.html.twig', ['tip' => $tipRepository->getRandomTip()]);
    }
}
