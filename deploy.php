<?php
namespace Deployer;

require 'recipe/symfony4.php';
require 'vendor/deployer/recipes/recipe/yarn.php';

// Cpanel Specific
set('bin/php', function () {
    return 'php';
});

set('bin/composer', function () {
    return 'php ~/composer.phar';
});
set('composer_options', '{{composer_action}} -d {{release_path}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');

// Project name
set('application', 'glean');
set('keep_releases', 3);

// Project repository
set('repository', 'git@bitbucket.org:sbernini/chooser.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', ['public/uploads','var/log']);

// Writable dirs by web server
add('writable_dirs', ['var/cache','var/log','var/sessions']);
set('writable_mode','chmod');
set('writable_use_sudo', false);
set('http_user', 'glean');
set('allow_anonymous_stats', false);

// Hosts
host('192.168.1.200')
    ->stage('production')
    ->user('glean')
    ->set('deploy_path', '~/{{application}}');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');
after('deploy:update_code', 'dump-env');
after('deploy:update_code', 'yarn:install');
before('deploy:symlink', 'yarn:build');
after('deploy:symlink', 'link:public');

task('dump-env', function () {
    run('alias php=/usr/local/bin/ea-php81');
    run('cd {{release_path}} && cp config/packages/prod/.env .');
    run('cd {{release_path}} && echo {{bin/composer}} dump-env prod');
})->onStage('production');

task('yarn:build', function() {
    run('cd {{release_path}} && yarn build');
});

task('link:public', function() {
    run('rm ~/public_html');
    run('ln -s ~/glean/current/public/ ~/public_html');
});
